"""
Praktikum Digitalisierung

Kalorimetrie - Küchentischversuche

@author: Bianca Beer
"""

# Module importieren
from functions import m_json
from functions import m_pck

def messungsaufzeichnung(json_folder: str, setup_path: str, data_folder: str) -> None:
    """
    Diese Funktion führt einen Versuch auf Grundlage eines gegebenen Setups durch und
    speichert die gemessenen Daten ab, indem sie das Setup-File ausliest und als Runtime
    Metadaten verwendet, damit die Messungen durchführt und die Ergebnisse in einem Ordner
    als HDF5-Datei speichert zusammen mit den dazugehörigen JSON-Files.
    
    Argumente:
        json_folder (str): Pfad zu dem Ordner, der die JSON-Files beinhaltet
        setup_path (str): Pfad zum zugrundeliegenden Setup-File
        data_folder (str): Pfad zu dem Ordner, wo die Ergebnisse gespeichert werden sollen
    """
    
    # Runtime Metadaten aus Setup- und Sensor-Files auslesen
    metadata = m_json.get_metadata_from_setup(setup_path)
    m_json.add_temperature_sensor_serials(json_folder, metadata)
    
    # Messdatenaufzeichnung
    data = m_pck.get_meas_data_calorimetry(metadata)

    # Abspeichern der Daten als HDF5 und zugehörigen JSON-Files
    m_pck.logging_calorimetry(data, metadata, data_folder, json_folder)
    m_json.archiv_json(json_folder, setup_path, data_folder)
    
# Pfad zu dem Ordner, der die JSON-Files beinhaltet
json_folder = "datasheets"

# ======= Wärmekapazität eines Behälters =======

# Definition der Pfade 
setup_path = "datasheets/setup_heat_capacity.json"
data_folder = "data/heat_capacity"

# Versuchsdurchführung
messungsaufzeichnung(json_folder, setup_path, data_folder)

# ======= Übergang zum nächsten Versuch =======
# Warten auf doppelte Benutzereingabe zum Fortführen des Skripts
input("Press two times ENTER to continue with the next experiment.")
input("Press ENTER to continue with the next experiment.")

# ======= Thermische Verluste (Newtonsches Abkühlungsgesetz) =======

# Definition der Pfade 
setup_path = "datasheets/setup_newton.json"
data_folder = "data/newton"

# Versuchsdurchführung
messungsaufzeichnung(json_folder, setup_path, data_folder)